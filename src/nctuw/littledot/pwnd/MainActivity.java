package nctuw.littledot.pwnd;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;

import nctuw.littledot.cipher.Cipher;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	private TextView tvValue;
	private EditText etCode;

	private Cipher mCipher;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_main);

		mCipher = new Cipher();

		tvValue = (TextView) findViewById(R.id.et_value);
		etCode = (EditText) findViewById(R.id.et_code);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.act_main, menu);
		return true;
	}

	public void onDecode(View v) {
		String text;
		long value = mCipher.getValue(etCode.getText().toString().trim());

		if (value == Cipher.INVALID_CODE) {
			text = "INVALID CODE, please try again";
		} else {
			BigDecimal bd = new BigDecimal((double) value / 100);
			NumberFormat format = NumberFormat.getCurrencyInstance();
			text = "Value=" + format.format(bd.doubleValue());
		}

		tvValue.setText(text);
	}
}
