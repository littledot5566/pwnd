package nctuw.littledot.cipher;

import java.util.Locale;

public class Cipher {
	public final static int	INVALID_CODE	= -1;

	private String					mCipher;

	public Cipher() {
		mCipher = Key.PAWN_AMERICA;
	}

	public Cipher(String key) {
		setCipher(key);
	}

	public void setCipher(String key) {
		mCipher = key;
	}

	public long getValue(String code) {
		int index;
		long value = 0;

		code = code.toUpperCase(Locale.US);

		for (int i = 0; i < code.length(); i++) {
			index = mCipher.indexOf(code.charAt(i));
			if (index == -1) {
				return INVALID_CODE;
			} else {
				value += index * Math.pow(10, code.length() - 1 - i);
			}
		}

		return value;
	}
}
